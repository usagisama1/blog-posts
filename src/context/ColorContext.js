import React, { useState, createContext } from "react";

export const ColorContext = createContext();

export const ColorProvider = (props) => {
  const [isDarkTheme, setIsDarkTheme] = useState(true);

  return (
    <ColorContext.Provider
      value={{
        isDarkTheme,
        setIsDarkTheme
      }}
    >
      {props.children}
    </ColorContext.Provider>
  );
};
