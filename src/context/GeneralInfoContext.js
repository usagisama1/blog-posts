import React, { useState, createContext } from "react";

export const GeneralInfoContext = createContext();

export const GeneralInfoProvider = (props) => {
  const [isLoading, setIsLoading] = useState(true);

  return (
    <GeneralInfoContext.Provider
      value={{
        isLoading,
        setIsLoading
      }}
    >
      {props.children}
    </GeneralInfoContext.Provider>
  );
};
