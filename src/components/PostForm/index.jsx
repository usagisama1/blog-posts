import React, { useState, useContext, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import Spinner from "../Spinner";

//Services
import { GetPostById, CreatePost, ModifyPost } from "../../services";

//Contexts
import { GeneralInfoContext } from "../../context/GeneralInfoContext";
import { ColorContext } from "../../context/ColorContext";

const PostForm = ({ isCreation }) => {
  const [post, setPost] = useState({
    title: "",
    body: "",
    userId: 1,
  });
  const { isLoading, setIsLoading } = useContext(GeneralInfoContext);
  const { isDarkTheme } = useContext(ColorContext);
  let { id } = useParams();
  let history = useHistory();

  const manageError = () => {
    history.push("/error");
  };
  
  const onClickExecute = async (post) => {
    setIsLoading(true);
    const response = (await isCreation) ? CreatePost(post) : ModifyPost(post);
    if (response.hasError) {
      manageError();
    } else {
      Swal.fire({
        icon: "success",
        title: isCreation
          ? `The post was successfully created`
          : `The post ${post.id} was successfully modified`,
        showConfirmButton: false,
        timer: 2000,
      });
      history.push("/");
    }
    setIsLoading(false);
  };

  const createForm = (e) => {
    e.preventDefault();
    onClickExecute(post);
  };

  const updateForm = (e) => {
    setPost({
      ...post,
      [e.target.name]: e.target.value,
    });
  };

  

  useEffect(async () => {
    if (!isCreation) {
      setIsLoading(true);
      const response = await GetPostById(id);
      response.hasError ? manageError() : setPost(response.post);
    }
    setIsLoading(false);
  }, []);

  return (
    <>
      {isLoading ? (
        <Spinner />
      ) : (
        <Form
          onSubmit={createForm}
          className={isDarkTheme ? "text-dark" : "text-light"}
        >
          <Form.Group>
            <Form.Label>Post's title</Form.Label>
            <Form.Control
              type="text"
              placeholder="e.g.: Your title"
              name="title"
              onChange={updateForm}
              value={post.title}
              maxLength={100}
              required
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Post's body</Form.Label>
            <Form.Control
              as="textarea"
              rows={10}
              placeholder="e.g.: Your post content"
              name="body"
              onChange={updateForm}
              value={post.body}
              maxLength={300}
              required
            />
          </Form.Group>
          <Button type="submit" variant="success">
            {isCreation ? "CREATE" : "MODIFY"}
          </Button>
        </Form>
      )}
    </>
  );
};

export default PostForm;
