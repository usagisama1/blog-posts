import React, { useContext } from "react";
import { Navbar, Button } from "react-bootstrap";
import HeaderLink from "../HeaderLink";

//Contexts
import { ColorContext } from "../../context/ColorContext";

const Header = () => {
  const { isDarkTheme, setIsDarkTheme } = useContext(ColorContext);

  const changeTheme = () => {
    setIsDarkTheme(!isDarkTheme);
  };
  return (
    <Navbar
      bg={isDarkTheme ? "dark" : "light"}
      variant={isDarkTheme ? "dark" : "light"}
      className="d-flex justify-content-between"
    >
      <div>
        <HeaderLink text="Home" routeLink="/" />
        <HeaderLink text="New" routeLink="/create" />
      </div>
      <Button variant={isDarkTheme ? "light" : "dark"} onClick={changeTheme}>
        {isDarkTheme ? "LIGHT" : "DARK"}
      </Button>
    </Navbar>
  );
};

export default Header;
