import React from "react";
import PostForm from '../PostForm';
const CreatePost = () => {
  return <PostForm isCreation={true}/>;
};

export default CreatePost;