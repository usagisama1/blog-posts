import React from "react";
import { Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt, faPencilAlt, faSearchPlus } from "@fortawesome/free-solid-svg-icons";
const GeneralButton = ({ onClickFunction, iconType }) => {
  let icon;
  let btnClass;
  switch (iconType) {
    case "MODIFY":
      icon = faPencilAlt;
      btnClass = "warning";
      break;
    case "DELETE":
      icon = faTrashAlt;
      btnClass = "danger";
      break;
    case "DETAIL":
      icon = faSearchPlus;
      btnClass = "success";
      break;
    default:
      icon = faSearchPlus;
      btnClass = "success";
      break;
  }
  return (
    <Button onClick={() => onClickFunction()} variant={btnClass} className='ml-2'>
      <FontAwesomeIcon icon={icon}/>
    </Button>
  );
};

export default GeneralButton;
