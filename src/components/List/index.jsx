import React, { useEffect, useContext, useState } from "react";
import { Table } from "react-bootstrap";
import { useHistory } from "react-router-dom";
//Contexts
import { GeneralInfoContext } from "../../context/GeneralInfoContext";
import { ColorContext } from "../../context/ColorContext";

//Components import
import ListItem from "../ListItem";
import Spinner from "../Spinner";

import { GetPosts } from "../../services";

const List = () => {
  const { isLoading, setIsLoading } = useContext(GeneralInfoContext);
  const { isDarkTheme } = useContext(ColorContext);
  const [postList, setPostList] = useState([]);
  let history = useHistory();

  useEffect(async () => {
    setIsLoading(true);
    const response = await GetPosts();
    response.hasError ? history.push("/error") : setPostList(response.posts);
    setIsLoading(false);
  }, []);

  return (
    <>
      {isLoading ? (
        <Spinner />
      ) : (
        <>
          {postList.length > 0 ? (
            <Table
              striped
              bordered
              hover
              variant={isDarkTheme ? "dark" : "light"}
            >
              <thead>
                <tr className="text-center">
                  <th>POST TITLE</th>
                  <th>ACTIONS</th>
                </tr>
              </thead>
              <tbody>
                {postList.map((post) => (
                  <ListItem key={post.id} post={post} />
                ))}
              </tbody>
            </Table>
          ) : (
            <h1>There are no posts</h1>
          )}
        </>
      )}
    </>
  );
};

export default List;
