import React, { useContext } from "react";
import { Link } from "react-router-dom";

//Contexts
import { ColorContext } from "../../context/ColorContext";

const HeaderLink = ({ text, routeLink }) => {
  const { isDarkTheme } = useContext(ColorContext);
  return (
    <Link to={routeLink} className={isDarkTheme?"mr-2 text-light":"mr-2 text-dark"}>
      {text}
    </Link>
  );
};

export default HeaderLink;
