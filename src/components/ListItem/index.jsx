import React from "react";
import ButtonGroup from "../ButtonGroup";

const ListItem = ({ post }) => {
  return (
    <tr>
      <td>{post.title}</td>
      <td className="d-flex justify-content-center">
        <ButtonGroup showDetail={true} showModify={true} showDelete={true} id={post.id}/>
      </td>
    </tr>
  );
};

export default ListItem;
