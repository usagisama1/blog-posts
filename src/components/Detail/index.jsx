import React, { useState, useEffect, useContext } from "react";
import { useParams, useHistory } from "react-router-dom";
import { Card } from "react-bootstrap";

import { GetPostById } from '../../services';

//Contexts
import { GeneralInfoContext } from "../../context/GeneralInfoContext";
import { ColorContext } from "../../context/ColorContext";

//Components import
import Spinner from "../Spinner";
import ButtonGroup from "../ButtonGroup";

const Detail = () => {
  const [post, setPost] = useState({
    title: "",
    body: "",
  });
  const { isLoading, setIsLoading } = useContext(GeneralInfoContext);
  const { isDarkTheme } = useContext(ColorContext);
  let { id } = useParams();
  let history = useHistory();

  useEffect(async () => {
    setIsLoading(true);
    const response = await GetPostById(id);
    response.hasError ? history.push("/error") : setPost(response.post);
    setIsLoading(false);
  }, []);

  return (
    <>
      {isLoading ? (
        <Spinner />
      ) : (
        <Card
          className={
            isDarkTheme
              ? "text-center bg-dark text-light"
              : "text-center bg-light text-dark"
          }
        >
          <Card.Body>
            <Card.Title>{post.title}</Card.Title>
            <Card.Text>{post.body}</Card.Text>
            {
              <ButtonGroup
                showDetail={false}
                showModify={true}
                showDelete={true}
                id={post.id}
              />
            }
          </Card.Body>
        </Card>
      )}
    </>
  );
};

export default Detail;
