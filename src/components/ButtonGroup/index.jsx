import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import GeneralButton from "../GeneralButton";
import { Modal, Button } from "react-bootstrap";
import Swal from "sweetalert2";

//Services
import { DeletePost } from "../../services";

//Contexts
import { GeneralInfoContext } from "../../context/GeneralInfoContext";

const ButtonGroup = ({ showDetail, showModify, showDelete, id }) => {
  const { setIsLoading } = useContext(GeneralInfoContext);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  let history = useHistory();

  const onClickModify = () => {
    history.push(`/post/modify/${id}`);
  };
  const onClickSeeDetail = () => {
    history.push(`/post/${id}`);
  };
  const onClickDelete = async () => {
    setIsLoading(true);
    const response = await DeletePost(id);
    if (response.hasError) {
      history.push("/error");
    } else {
      Swal.fire({
        icon: "success",
        title: `The post ${id} was successfully deleted`,
        showConfirmButton: false,
        timer: 2000,
      });
      history.push("/");
    }
    setIsLoading(false);
  };

  return (
    <>
      {showDetail ? (
        <GeneralButton onClickFunction={onClickSeeDetail} iconType="DETAIL" />
      ) : (
        ""
      )}
      {showModify ? (
        <GeneralButton onClickFunction={onClickModify} iconType="MODIFY" />
      ) : (
        ""
      )}
      {showDelete ? (
        <GeneralButton onClickFunction={handleShow} iconType="DELETE" />
      ) : (
        ""
      )}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Post</Modal.Title>
        </Modal.Header>
        <Modal.Body>You sure to permanently delete post {id}?</Modal.Body>
        <Modal.Footer className="d-flex justify-content-center">
          <Button variant="danger" onClick={onClickDelete}>
            YES
          </Button>
          <Button variant="success" onClick={handleClose}>
            NO!
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ButtonGroup;
