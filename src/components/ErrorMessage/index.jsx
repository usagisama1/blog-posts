import React from "react";
import { Image } from "react-bootstrap";
import DogImage from "../../images/error.jpg";
const ErrorMessage = () => {
  return (
    <div className='d-flex justify-content-center flex-column text-center'>
      <h1>Something went wrong!</h1>
      <h5>Poor headache puppy...</h5>
      <Image src={DogImage} alt='error message'/>
    </div>
  );
};

export default ErrorMessage;
