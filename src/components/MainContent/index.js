import React, { useContext } from "react";
import { Switch, Route } from "react-router-dom";
import { Container } from "react-bootstrap";

import "./style.css";

//Components imports
import Detail from "../Detail";
import CreatePost from "../CreatePost";
import ModifyPost from "../ModifyPost";
import List from "../List";
import ErrorMessage from "../ErrorMessage";
import Header from "../Header";

//Contexts
import { ColorContext } from "../../context/ColorContext";

const MainContent = () => {
  const { isDarkTheme } = useContext(ColorContext);
  return (
    <div
      className={
        isDarkTheme
          ? "bg-light main-content-container"
          : "bg-dark main-content-container"
      }
    >
      <Header />
      <Container className="mt-3">
        <Switch>
          <Route exact path="/post/:id">
            <Detail />
          </Route>
          <Route exact path="/post/modify/:id">
            <ModifyPost />
          </Route>
          <Route exact path="/create">
            <CreatePost />
          </Route>
          <Route exact path="/error">
            <ErrorMessage />
          </Route>
          <Route exact path="/">
            <List />
          </Route>
          <Route path="/">
            <ErrorMessage />
          </Route>
        </Switch>
      </Container>
    </div>
  );
};

export default MainContent;
