import axios from "axios";

export const GetPosts = () =>
  axios
    .get("https://jsonplaceholder.typicode.com/posts")
    .then((response) => {
      return {
        posts: response.data,
        hasError: false,
      };
    })
    .catch((error) => {
      return {
        error: error.message,
        hasError: true,
      };
    });

export const GetPostById = (id) =>
  axios
    .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
    .then((response) => {
      return {
        post: response.data,
        hasError: false,
      };
    })
    .catch((error) => {
      return {
        error: error.message,
        hasError: true,
      };
    });

export const CreatePost = (post) =>
  axios
    .post("https://jsonplaceholder.typicode.com/posts", post)
    .then((response) => {
      return {
        hasError: false,
        createdPost: response.data,
      };
    })
    .catch((error) => {
      return {
        error: error.message,
        hasError: true,
      };
    });

export const ModifyPost = (post) =>
  axios
    .patch(`https://jsonplaceholder.typicode.com/posts/${post.id}`, post)
    .then((response) => {
      return {
        hasError: false,
        modifiedPost: response.data,
      };
    })
    .catch((error) => {
      return {
        error: error.message,
        hasError: true,
      };
    });

export const DeletePost = (id) =>
  axios
    .delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
    .then((response) => {
      return {
        hasError: false,
      };
    })
    .catch((error) => {
      return {
        error: error.message,
        hasError: true,
      };
    });
