import React from "react";
import { BrowserRouter as Router } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./style.css";

//Components
import MainContent from "./components/MainContent";

//Contexts
import { ColorProvider } from "./context/ColorContext";
import { GeneralInfoProvider } from "./context/GeneralInfoContext";

function App() {
  return (
    <Router>
      <ColorProvider>
        <GeneralInfoProvider>
          <MainContent />
        </GeneralInfoProvider>
      </ColorProvider>
    </Router>
  );
}

export default App;
